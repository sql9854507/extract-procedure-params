CREATE PROCEDURE [api].[sp_getProcedureParams]
	@ProcedureName varchar(255),
	@Schema varchar(30) = 'dbo',
	@EntityStateCode varchar(30) = null OUTPUT,
	@EntityGroupCode varchar(30) = null OUTPUT
AS

SET NOCOUNT ON;
SET XACT_ABORT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN
	;WITH sql AS
	(
		SELECT 
			OBJECT_DEFINITION (OBJECT_ID(CONCAT_WS('.', DB_NAME(), @Schema, @ProcedureName))) AS syntax
	),

	header AS
	(
		SELECT
			SUBSTRING(
				syntax, 
				PATINDEX('%CREATE PROCEDURE%', syntax) + LEN('CREATE PROCEDURE') + LEN(CONCAT_WS('.', QUOTENAME(@Schema, '[]'), QUOTENAME(@ProcedureName, '[]'))) + 1, 
				PATINDEX('%' + CHAR(13) + CHAR(10) + 'AS' + CHAR(13) + CHAR(10) + '%', syntax) - (PATINDEX('%CREATE PROCEDURE%', syntax) + LEN('CREATE PROCEDURE') + LEN(CONCAT_WS('.', QUOTENAME(@Schema, '[]'), QUOTENAME(@ProcedureName, '[]'))) + 1)
			) syntax
		FROM
			sql
	),

	syntaxRows AS
	(
		SELECT
			TRIM(REPLACE(REPLACE(CASE PATINDEX('%--%', value)
				WHEN 0 THEN value
				ELSE LEFT(value, PATINDEX('%--%', value) - LEN('--'))
			END, ' AS ', ' '), char(10), '')) param
		FROM
			string_split((SELECT syntax FROM header), CHAR(13)) paramsSplit
	),

	syntaxParams AS
	(
		SELECT
			SUBSTRING(param, PATINDEX('%@%', param), PATINDEX('% %', param) - PATINDEX('%@%', param) + 1) param,
			CASE
				WHEN PATINDEX('%=%', param) > 0 THEN TRIM(TRANSLATE(REPLACE(RIGHT(param, CHARINDEX('=', REVERSE(param))), ' OUTPUT',''), CHAR(9) + CHAR(10) + CHAR(13) + '=,', '     '))
			END as defaultValue,
			1 - SIGN(CHARINDEX('=', REVERSE(param))) AS isRequired
		FROM
			syntaxRows
	),

	sysParams AS
	(
		SELECT
			parameters.name AS param,
			types.name AS dataType,
			parameters.max_length AS size,
			parameters.precision,
			parameters.scale,
			parameters.is_output AS isOutput
		FROM
			sys.objects
			JOIN sys.schemas
				ON objects.schema_id = schemas.schema_id
			JOIN sys.parameters
				ON objects.object_id = parameters.object_id
			JOIN sys.types
				ON types.system_type_id = parameters.system_type_id
					AND types.user_type_id = parameters.user_type_id
		WHERE
			schemas.name = @Schema
			AND objects.name = @ProcedureName
	)

	SELECT
		sysParams.param,
		sysParams.dataType,
		sysParams.size,
		sysParams.precision,
		sysParams.scale,
		sysParams.isOutput,
		syntaxParams.isRequired,
		syntaxParams.defaultValue
	FROM
		syntaxParams
		JOIN sysParams
			ON syntaxParams.param = sysParams.param

END
