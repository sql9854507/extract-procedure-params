CREATE PROCEDURE [dbo].[sp_getProcedureParams]
	@ProcedureName varchar(255),
	@Schema varchar(30) = 'dbo',
	@EntityStateCode varchar(30) = null OUTPUT,
	@EntityGroupCode varchar(30) = null OUTPUT
AS

SET NOCOUNT ON;
SET XACT_ABORT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN
	;WITH sql AS
	(
		SELECT 
			OBJECT_DEFINITION (OBJECT_ID(CONCAT_WS('.', DB_NAME(), @Schema, @ProcedureName))) AS syntax
	),

	header AS
	(
		SELECT
			SUBSTRING(
				syntax, 
				PATINDEX('%CREATE PROCEDURE%', syntax) + LEN('CREATE PROCEDURE') + LEN(CONCAT_WS('.', QUOTENAME(@Schema, '[]'), QUOTENAME(@ProcedureName, '[]'))) + 1, 
				PATINDEX('%' + CHAR(13) + CHAR(10) + 'AS' + CHAR(13) + CHAR(10) + '%', syntax) - (PATINDEX('%CREATE PROCEDURE%', syntax) + LEN('CREATE PROCEDURE') + LEN(CONCAT_WS('.', QUOTENAME(@Schema, '[]'), QUOTENAME(@ProcedureName, '[]'))) + 1)
			) syntax
		FROM
			sql
	),

	paramsRow AS
	(
		SELECT
			REPLACE(CASE PATINDEX('%--%', value)
				WHEN 0 THEN value
				ELSE LEFT(value, PATINDEX('%--%', value) - 2)
			END, ' AS ', ' ') param
		FROM
			string_split((SELECT syntax FROM header), CHAR(13)) paramsSplit
	),

	paramInfo as (
		SELECT
			TRIM(paramInfo.value) value,
			DENSE_RANK() OVER(ORDER BY paramsRow.param) paramId,
			rn
		FROM
			paramsRow
			CROSS APPLY (
				SELECT
					REPLACE(REPLACE(value, char(10), ''), char(9), '') value,
					ROW_NUMBER() OVER(PARTITION BY paramsRow.param ORDER BY (SELECT NULL)) rn
				FROM
					string_split(paramsRow.param, ' ')
				) paramInfo
		WHERE
			TRIM(paramInfo.value) NOT IN (char(13), char(10), char(9), '')
	)

	SELECT
		REPLACE(REPLACE(TRIM(MAX(IIF(rn = 1, value, null))), ',', ''), '@', '') AS Parameter,
		REPLACE(TRIM(MAX(IIF(rn = 2, value, null))), ',', '') AS DataType,
		REPLACE(REPLACE(TRIM(MAX(IIF(rn = 4, value, null))), ',', ''), '''', '') AS DefalutValue,
		CASE
			WHEN REPLACE(TRIM(MAX(IIF(rn = 4, value, null))), ',', '') IS NULL THEN 1
			ELSE 0
		END as Required,
		CASE
			WHEN REPLACE(TRIM(MAX(IIF(rn = 3, value, null))), ',', '') = 'OUTPUT' THEN 1
			WHEN REPLACE(TRIM(MAX(IIF(rn = 5, value, null))), ',', '') = 'OUTPUT' THEN 1
			ELSE 0
		END AS Output
	FROM
		paramInfo
	GROUP BY
		paramInfo.paramId

END
